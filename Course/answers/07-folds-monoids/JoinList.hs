--{-# OPTIONS_GHC -Wall #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}

module JoinList where

import Sized
import Data.Functor
import Data.Monoid
import Data.Maybe
import Control.Applicative
import Test.QuickCheck 
import Test.QuickCheck.All 

data JoinList m a = Empty
    | Single m a
    | Append m (JoinList m a) (JoinList m a)
  deriving (Eq, Show)

instance (Arbitrary a, Sized s, Monoid s, Num s) => Arbitrary (JoinList s a) where
  arbitrary = sized jTree

jTree n | n>0 = oneof [ Single 1 <$> arbitrary
                      , (+++) <$> jTree (n `div` 2) <*> jTree (n `div` 2)
                      ]
    
tag :: Monoid m => JoinList m a -> m
tag Empty = mempty
tag (Single m _) = m
tag (Append m _ _) = m

(+++) :: Monoid m => JoinList m a -> JoinList m a -> JoinList m a
jl1 +++ jl2 = Append (tag jl1 `mappend` tag jl2) jl1 jl2

example :: JoinList (Product Integer) Char
example = Append (Product 210)
           (Append (Product 30)
             (Single (Product 5) 'y')
             (Append (Product 6)
               (Single (Product 2) 'e')
               (Single (Product 3) 'a')))
           (Single (Product 7) 'h')

simple :: JoinList (Sum Integer) Char
simple = Append (Sum 5) (Single (Sum 2) 'a') (Single (Sum 3) 'b')

sizeJ :: (Sized b, Monoid b) => JoinList b a -> Int
sizeJ = getSize . size . tag

jlToList :: JoinList m a -> [a]
jlToList Empty            = []
jlToList (Single _ a)     = [a]
jlToList (Append _ l1 l2) = jlToList l1 ++ jlToList l2

indexJ :: (Sized b, Monoid b) => Int -> JoinList b a -> Maybe a
indexJ _ Empty                               = Nothing
indexJ i _     | i <  0                         = Nothing
indexJ i jl    | i >= (getSize . size . tag) jl = Nothing
indexJ _ (Single _ x) = Just x
indexJ i (Append _ left right) 
               | i < leftSize = indexJ (i - rightSize) left
               | otherwise    = indexJ (i - leftSize) right
             where leftSize = (getSize . size . tag) left
                   rightSize = (getSize . size . tag) right

(!!?) :: Int -> [a] -> Maybe a
_ !!? []        = Nothing
i !!? _ | i < 0 = Nothing
0 !!? (x:_)    = Just x
i !!? (_:xs)    = (i-1) !!? xs

prop_indexJ :: (Eq a, Show a) => Int -> JoinList (Sum Int) a -> Bool
prop_indexJ i jl = indexJ i jl == i !!? jlToList jl

-- fails with:

-- 0
-- Append (Size 4) (Single (Size 2) '^') (Single (Size 2) 'B')

runTests :: IO Bool
runTests = $(quickCheckAll)

main :: IO ()
main = quickCheck prop_indexJ
